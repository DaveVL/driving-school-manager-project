var gulp = require('gulp'); gulp.task('default', function () { console.log('Hello Gulp!') });

gulp.task('watch', function() {
    gulp.watch( "./stylesheets/**/*.scss", ["stylesheets"] );
    gulp.watch( "./html/**/*.html", ["html"] );
});


var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('resources/assets/sass/app.scss', 'public/css');

	mix.scripts([
        'jquery-3.2.1.min.js',
		'popper.min.js',
		'bootstrap.min.js',
		'bootstrap-sprockets.js',
		'mdb.min.js',
		'jquery.dataTables.min.js',
		'datatables.bootstrap4.min.js',
		'/vendor/fullcalendar-3.6.2/lib/moment.min.js',
        '/vendor/fullcalendar-3.6.2/fullcalendar.min.js',
        '/vendor/fullcalendar-3.6.2/locale-all.js',
    ]);
	
	mix.browserify('main.js');
});

