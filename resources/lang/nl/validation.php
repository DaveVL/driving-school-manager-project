`<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted"             => "Veld dient te worden geaccepteerd.",
    "active_url"           => "Veld is geen geldige URL.",
    "after"                => "Veld dient een datum te zijn na :date.",
    "after_or_equal"       => 'Veld moet een datum zijn na of gelijk aan :date.',
    "alpha"                => "Veld mag alleen letters bevatten.",
    "alpha_dash"           => "Veld mag alleen letters, nummers, and strepen bevatten.",
    "alpha_num"            => "Veld mag alleen letters en nummers bevatten.",
    "array"                => "Veld moet een array zijn.",
    "before"               => "Veld moet een datum zijn eerder dan :date.",
    "before_or_equal"      => 'Veld moet een datum zijn voor of gelijk aan :date.',
    "between"              => [
        "numeric" => "Veld moet tussen :min en :max liggen.",
        "file"    => "Veld moet tussen :min en :max kilobytes zijn.",
        "string"  => "Veld moet tussen :min en :max karakters lang zijn.",
        "array"   => "Veld moet tussen :min en :max items bevatten.",
    ],
    "boolean"              => "Veld kan enkel true of false zijn.",
    "confirmed"            => "Veld bevestiging komt niet overeen.",
    "date"                 => "Veld is geen geldige datum.",
    "date_format"          => "Veld komt niet overeen met het formaat :format.",
    "different"            => "Veld en :other dienen verschillend te zijn.",
    "digits"               => "Veld moet :digits cijfers zijn.",
    "digits_between"       => "Veld moet tussen :min en :max cijfers zijn.",
    "dimensions"           => "Veld heeft een ongeldige grootte.",
    "distinct"             => "Veld heeft een dubbele waarde.",
    "email"                => "Veld dient een geldig emailadres te zijn.",
    "exists"               => "Het geselecteerde veld is ongeldig.",
    "file"                 => "Veld moet een bestand zijn.",
    "filled"               => "Veld veld is verplicht.",
    "image"                => "Veld dient een afbeelding te zijn.",
    "in"                   => "Het geselecteerde Veld is ongeldig.",
    "in_array"             => "Veld komt niet voor in :other.",
    "integer"              => "Veld dient een geheel getal te zijn.",
    "ip"                   => "Veld dient een geldig IP adres te zijn.",
    "json"                 => "Veld moet een geldige JSON string zijn.",
    "max"                  => [
        "numeric" => "Veld mag niet groter zijn dan :max.",
        "file"    => "Veld mag niet groter zijn dan :max kilobytes.",
        "string"  => "Veld mag niet groter zijn dan :max karakters.",
        "array"   => "Veld mag niet meer dan :max items bevatten.",
    ],
    "mimes"                => "Veld dient een bestand te zijn van het type: :values.",
    "mimetypes"            => "Veld dient een bestand te zijn van het type: :values.",
    "min"                  => [
        "numeric" => "Veld dient minimaal :min te zijn.",
        "file"    => "Veld dient minimaal :min kilobytes te zijn.",
        "string"  => "Veld dient minimaal :min karakters te bevatten.",
        "array"   => "Veld dient minimaal :min items te bevatten.",
    ],
    "not_in"               => "Het geselecteerde Veld is ongeldig.",
    "numeric"              => "Deze veldnaam dient een nummer te zijn.",
    "present"              => "Deze veldnaam dient aanwezig te zijn.",
    "regex"                => "Deze veldnaam formaat is ongeldig.",
    "required"             => "Deze veldnaam veld is verplicht.",
    "required_if"          => "Deze veldnaam veld is verplicht wanneer :other is :value.",
    "required_unless"      => "Deze veldnaam is verplicht, tenzij :other is in :values.",
    "required_with"        => "Deze veldnaam veld is verplicht wanneer :values aanwezig is.",
    "required_with_all"    => "Deze veldnaam veld is verplicht wanneer :values aanwezig is.",
    "required_without"     => "Deze veldnaam veld is verplicht wanneer :values niet aanwezig is.",
    "required_without_all" => "Deze veldnaam veld is verplicht wanneer geen van :values aanwezig is.",
    "same"                 => "De veld waarmee het vergelijkt moet worden komen niet over een.",
    "size"                 => [
        "numeric" => "Veld moet :size zijn.",
        "file"    => "Veld moet :size kilobytes groot zijn.",
        "string"  => "Veld moet :size karakters lang zijn.",
        "array"   => "Veld moet :size items bevatten.",
    ],
    "string"               => "Veld moet een string zijn.",
    "timezone"             => "Veld moet een geldige tijdszone zijn.",
    "unique"               => "Veld is al bezet.",
    "uploaded"             => "Het uploaden van Veld is mislukt.",
    "url"                  => "Veld formaat is ongeldig.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],
];
