<?php
return [
    'delete_vehicle_part_one_warning' => 'Weet u zeker dat u het voertuig met kentekenplaat ',
    'delete_vehicle_part_two_warning' => ' wilt verwijderen?',

    'delete_vehicle_brand_part_one_warning' => 'Weet u zeker dat u het merk ',
    'delete_vehicle_brand_part_two_warning' => ' wilt verwijderen?',

    'delete_vehicle_model_part_one_warning' => 'Weet u zeker dat u het model ',
    'delete_vehicle_model_part_two_warning' => ' wilt verwijderen?',

    'delete_vehicle_category_part_one_warning' => 'Weet u zeker dat u de categorie ',
    'delete_vehicle_category_part_two_warning' => ' wilt verwijderen?',

    'archive_user_part_one_warning' => 'Weet u zeker dat u ',
    'archive_user_part_two_warning' => ' wilt archiveren? Klik ja om de gebruiker te archiveren.',

    'archive_ticket_part_one' => 'Weet u zeker dat u de onderstaande tickets with archiveren?',
    'archive_ticket_part_two' => 'Klik ja om de tickets te archiveren.',

    'delete_role_part_one_warning' => 'Weet u zeker dat u de rol ',
    'delete_role_part_two_warning' => ' wilt verwijderen? Wanneer verwijderd, kan de rol niet teruggehaald worden.',

    'delete_support_category_part_one_warning' => 'Weet u zeker dat u de categorie ',
    'delete_support_category_part_two_warning' => ' wilt verwijderen? Wanneer verwijderd zullen ook de gerelateerde sub categorieën verwijderd worden.',

    'delete_support_sub_category_part_one_warning' => 'Weet u zeker dat u de sub categorie ',
    'delete_support_sub_category_part_two_warning' => ' wilt verwijderen?',

    'delete_support_priority_part_one_warning' => 'Weet u zeker dat u de prioriteit ',
    'delete_support_priority_part_two_warning' => ' wilt verwijderen?',

    'delete_support_bookmark_part_one_warning' => 'Weet u zeker dat u de markering ',
    'delete_support_bookmark_part_two_warning' => ' wilt verwijderen?',

];
