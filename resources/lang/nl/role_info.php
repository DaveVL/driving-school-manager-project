<?php

return [
  'vehicles_access_info' => 'Rol heeft toegang tot de voertuigen pagina en kan voertuigen bekijken.',
  'vehicles_create_info' => 'Rol kan voertuigen aanmaken.',
  'vehicles_edit_info' => 'Rol kan voertuigen bewerken.',
  'vehicles_delete_info' => 'Rol kan voertuigen verwijderen.',

  'vehicle_categories_access_info' => 'Rol heeft toegang tot de voertuig categorieën pagina en kan categorieën bekijken.',
  'vehicle_categories_create_info' => 'Rol kan voertuig categorieën aanmaken.',
  'vehicle_categories_edit_info' => 'Rol kan voertuig categorieën bewerken.',
  'vehicle_categories_delete_info' => 'Rol kan voertuig categorieën verwijderen.',

  'materials_access_info' => 'Rol heeft toegang tot de materialen pagina en kan materialen bekijken.',
  'materials_create_info' => 'Rol kan materialen aanmaken.',
  'materials_edit_info' => 'Rol kan materialen bewerken.',
  'materials_delete_info' => 'Rol kan materialen verwijderen.',

  'employees_access_info' => 'Rol heeft toegang tot de gebruikers pagina en kan werknemers bekijken.',
  'employees_create_info' => 'Rol kan werknemers aanmaken.',
  'employees_edit_info' => 'Rol kan werknemers bewerken.',
  'employees_archive_info' => 'Rol kan werknemers archiveren.',

  'customers_access_info' => 'Rol heeft toegang tot de gebruikers pagina en kan klanten bekijken.',
  'customers_create_info' => 'Rol kan klanten aanmaken.',
  'customers_edit_info' => 'Rol kan klanten bewerken.',
  'customers_archive_info' => 'Rol kan klanten archiveren.',

  'clients_access_info' => 'Rol heeft toegang tot de gebruikers pagina en kan cliënten bekijken.',
  'clients_create_info' => 'Rol kan cliënten aanmaken.',
  'clients_edit_info' => 'Rol kan cliënten bewerken.',
  'clients_archive_info' => 'Rol kan cliënten archiveren.',

  'news_access_info' => 'Rol heeft toegang tot nieuws pagina en kan nieuws bekijken.',
  'news_create_info' => 'Rol kan nieuws aanmaken.',
  'news_edit_info' => 'Rol kan nieuws bewerken.',
  'news_delete_info' => 'Rol kan nieuws verwijderen.',

  'files_access_info' => 'Rol heeft toegang tot de bestanden pagina en kan bestanden bekijken.',
  'files_create_info' => 'Rol kan bestanden aanmaken.',
  'files_edit_info' => 'Rol kan bestanden bewerken.',
  'files_delete_info' => 'Rol kan bestanden verwijderen.',

  'support_overview_access_info' => 'Rol heeft toegang tot de overzicht pagina.',
  'support_management_access_info' => 'Rol heeft toegang tot de beheer pagina.',
  'support_edit_info' => 'Rol kan tickets bewerken. (Hier geld ook markeren bij.)',
  'support_archive_info' => 'Rol kan tickets archiveren.',

  'support_create_category_info' => 'Rol kan support categorieën aanmaken.',
  'support_edit_category_info' => 'Rol kan support categorieën bewerken.',
  'support_delete_category_info' => 'Rol kan support categorieën verwijderen.',

  'support_create_sub_category_info' => 'Rol kan support sub categorieën aanmaken.',
  'support_edit_sub_category_info' => 'Rol kan support sub categorieën bewerken.',
  'support_delete_sub_category_info' => 'Rol kan support sub categorieën verwijderen.',

  'support_create_priority_info' => 'Rol kan support prioriteiten aanmaken.',
  'support_edit_priority_info' => 'Rol kan support prioriteiten bewerken.',
  'support_delete_priority_info' => 'Rol kan support prioriteiten verwijderen.',

  'support_create_bookmark_info' => 'Rol kan support markeringen aanmaken.',
  'support_edit_bookmark_info' => 'Rol kan support markeringen bewerken.',
  'support_delete_bookmark_info' => 'Rol kan support markeringen verwijderen.',

  'tickets_access_info' => 'Rol heeft toegang tot de ticket pagina en kan zijn eigen tickets bekijken.',
  'tickets_create_info' => 'Rol kan tickets creëeren',

  'activity_log_access_info' => 'Rol heeft toegang tot de activiteiten log pagina en kan logs bekijken.',
  'activity_log_print_info' => 'Rol kan activiteiten logs printen',
];
