<?php

return [

    'error_404_quote' => 'Ik vind het leuk om te denken dat, zelfs als we slechte keuzes maken en soms naar slechte paden gaan, we er uiteindelijk toch uitkomen.',

    'your_email' => 'Uw email',
    'your_password' => 'Uw wachtwoord',

    'dashboard' => 'Dashboard',
    'calendar' => 'Kalender',
    'vehicle_management' => 'Voertuig beheer',
    'vehicles' => 'Voertuigen',
    'categories' => 'Categorieën',
    'garage' => 'Garage',
    'repairs' => 'Reparaties',
    'apk' => 'APK',
    'materials' => 'Materialen',
    'users' => 'Gebruikers',
    'roles' => 'Rollen',
    'news' => 'Nieuws',
    'files' => 'Bestanden',
    'support' => 'Support',
    'support_overview' => 'Support overzicht',
    'support_management' => 'Support beheer',
    'tickets' => 'Tickets',
    'activity_log' => 'Activiteitenlog',

    'overview' => 'Overzicht',
    'management' => 'Beheer',

    'employees' => 'Werknemers',
    'employee' => 'Werknemer',

    'customers' => 'Klanten',
    'customer' => 'Klant',

    'clients' => 'Cliënten',
    'client' => 'Cliënt',

    'notifications' => 'Notificaties',
    'profile' => 'Profiel',
    'logout' => 'Log Uit',
    'my_account' => 'Mijn account',
    'my_profile' => 'Mijn profiel',

    'add' => 'Toevoegen',
    'create' => 'Creëeren',
    'edit' => 'Bewerken',
    'update' => 'Update',
    'save' => 'Opslaan',
    'cancel' => 'Cancel',
    'delete' => 'Verwijderen',
    'actions' => 'Acties',
    'search' => 'Zoek',
    'access' => 'Toegang',
    'print' => 'Print',
    'send' => 'Verzend',
    'reset' => 'Reset',

    'yes' => 'Ja',
    'no' => 'Nee',

    'next_step' => 'Volgende stap',
    'back' => 'Terug',

    'email' => 'Email',
    'first_name' => 'Voornaam',
    'last_name' => 'Achternaam',
    'address' => 'Adres',
    'address_information' => 'Adresgegevens',
    'city' => 'Stad',
    'zipcode' => 'Postcode',
    'state' => 'Staat',
    'province' => 'Provincie',
    'country' => 'Land',
    'phone' => 'Telefoon nr.',
    'mobile' => 'Mobiel nr.',
    'bsn_number' => 'BSN nummer',
    'birthday' => 'Geboortedatum',
    'date_of_service' => 'Datum in dienst',
    'date_of_subscription' => 'Datum van inschrijving',
    'password' => 'Wachtwoord',
    'old_password' => 'Oude wachtwoord',
    'new_password' => 'Nieuwe wachtwoord',
    'repeat_new_password' => 'Herhaal nieuwe wachtwoord',
    'repeat_password' => 'Herhaal wachtwoord',
    'reset_password' => 'Reset wachtwoord',
    'forgot_password' => 'Wachtwoord vergeten?',
    'reset_password_link_send' => 'We hebben uw wachtwoord reset link verstuurd!',

    'title' => 'Titel',
    'description' => 'Descriptie',
    'content' => 'Inhoud',

    'posted_by' => 'Gepost door',
    'created_at' => 'Gemaakt op',
    'updated_at' => 'Bewerkt op',

    'avatar' => 'Avatar',
    'profile_photo_text' => 'Profiel foto wordt automatisch gewijzigd.',
    'upload_new_photo' => 'Upload nieuwe foto',

    'personal_data' => 'Persoonsgegevens',

    'number' => 'Nummer',

    'vehicle_data' => 'Voertuiggegevens',
    'license_plate' => 'Kenteken',
    'purchase_date' => 'Koopdatum',
    'sales_date' => 'Verkoopdatum',
    'under_repair' => 'Onder reparatie',
    'next_apk' => 'Volgende APK',

    'note' => 'Notitie',

    'name' => 'Naam',

    'driver_license' => 'Rijbewijs',

    'brands' => 'Merken',
    'brand' => 'Merk',

    'models' => 'Modellen',
    'model' => 'Model',

    'types' => 'Types',
    'type' => 'Type',

    'vehicle' => 'Voertuig',
    'create_vehicle' => 'Creeër voertuig',
    'edit_vehicle' => 'Bewerk voertuig',
    'create_vehicles' => 'Creëer voertuigen',
    'edit_vehicles' => 'Bewerk voertuigen',
    'delete_vehicles' => 'Verwijder voertuigen',

    'create_vehicle_brand' => 'Creëer voertuig merk',
    'edit_vehicle_brand' => 'Bewerk voertuig merk',

    'create_vehicle_model' => 'Creëer voertuig model',
    'edit_vehicle_model' => 'Bewerk voertuig model',

    'create_vehicle_category' => 'Creeër voertuig categorie',
    'edit_vehicle_category' => 'Bewerk voertuig categorie',

    'create_material' => 'Creëer materiaal',

    //USER
    'create_user' => 'Creëer',
    'reset_user_password' => 'Reset wachtwoord',
    'want_password_reset' => 'Wilt u uw wachtwoord veranderen?',

    'create_employee' => 'Creëer werknemer',
    'edit_employee' => 'Bewerk werknemer',

    'create_customer' => 'Creëer klant',
    'edit_customer' => 'Bewerk klant',

    'create_client' => 'Creëer cliënt',
    'edit_client' => 'Bewerk cliënt',

    'role' => 'Rol',
    'create_role' => 'Creëer rol',
    'edit_role' => 'Bewerk rol',

    'create_news' => 'Creëer nieuwsbericht',
    'news_items' => 'Nieuwsberichten',

    'new_category' => 'Nieuwe categorie',
    'category' => 'Categorie',
    'create_categories' => 'Creëer categorieën',
    'edit_categories' => 'Bewerk categorieën',
    'delete_categories' => 'Verwijder categorieën',
    'new_sub_category' => 'Nieuwe subcategorie',
    'sub_categories' => 'Subcategorieën',
    'sub_category' => 'Subcategorie',
    'create_sub_categories' => 'Creëer subcategorieën',
    'edit_sub_categories' => 'Bewerk subcategories',
    'delete_sub_categories' => 'Verwijder subcategorieën',

    'selected' => 'Geselecteerd',
    'sort_on' => 'Sorteer Op',
    'select' => 'Selecteer',
    'deselect' => 'Deselecteren',

    'create_support_category' => 'Creëer categorie',
    'create_support_sub_category' => 'Creëer subcategorie',
    'create_support_priority' => 'Creëer prioriteit',
    'create_support_bookmark' => 'Creëer  markering',

    'edit_support_category' => 'Bewerk categorie',
    'edit_support_sub_category' => 'Bewerk subcategorie',
    'edit_support_priority' => 'Bewerk prioriteit',
    'edit_support_bookmark' => 'Bewerk markering',

    'delete_support_priority' => 'Verwijder prioriteit',
    'delete_support_bookmark' => 'Verwijder markering',

    'see_results' => 'Resultaten',

    'bookmark' => 'Markeer',
    'bookmarking' => 'Markering',
    'bookmarks' => 'Markerings',
    'mark_as' => 'Markeer als',
    'marked_as' =>'Gemarkeerd als',
    'to_mark' => 'Markeren',

    'active' => 'Actief',
    'archive' => 'Archief',
    'archiving' => 'Archiveren',

    'info' => 'Informatie',

    'bulk_actions' => 'Bulkacties',
    'export' => 'Export',
    'show_only' => 'Laat alleen zien',
    'all' => 'Allemaal',
    'solved' => 'Opgelost',
    'unsolved' => 'Onopgelost',

    'filter_segments' => 'Filter segmenten',
    'global' => 'Globaal',
    'related' => 'Gerelateerd',
    'related_to' => 'Gerelateerd aan',

    'priority' => 'Prioriteit',
    'priorities' => 'Prioriteiten',

    'status' => 'Status',

    'ticket' => 'Ticket',
    'new_ticket' => 'Nieuwe ticket',
    'my_tickets' => 'Mijn tickets',
    'create_ticket' => 'Creëer ticket',

    'expiry_date' => 'Uiterste datum',

    'stock' => 'Voorraad',

    //LOG
    'executor' => 'Uitvoerder',
    'activity' => 'Activiteit',
    'related_user_id' => 'gerelateerde gebruiker id',
    'section_model' => 'Section model',
    'section_target' => 'Section target',
];
