<?php

return [
    'illegal_conventions' => 'U heeft foutieve waardes ingevuld.',
    'unexpected_error' => 'Verwerking niet gelukt. Er is een onverwachte foutmelding gekomen.',

    'my_profile_updated' => 'Uw profiel is succesvol geüpdate.',

    'vehicle_created' => 'Voertuig is succesvol aangemaakt.',
    'vehicle_modified' => 'Voertuig is succesvol bewerkt.',
    'vehicle_deleted' => 'Voertuig is succesvol verwijderd.',

    'vehicle_brand_created' => 'Merk is succesvol aangemaakt.',
    'vehicle_brand_modified' => 'Merk is succesvol bewerkt.',
    'vehicle_brand_deleted' => 'Merk is succesvol verwijderd.',
    'vehicle_brand_is_used' => 'Merk wordt gebruikt door voertuig modellen. kan niet verwijderd worden.',

    'vehicle_model_created' => 'Model is succesvol aangemaakt.',
    'vehicle_model_modified' => 'Model is succesvol bewerkt.',
    'vehicle_model_deleted' => 'Model is succesvol verwijderd.',
    'vehicle_model_is_used' => 'Model is verbonden aan een voertuig. Kan niet verwijderd worden.',

    'vehicle_category_created' => 'Categorie is succesvol aangemaakt.',
    'vehicle_category_modified' => 'Categorie is succesvol bewerkt.',
    'vehicle_category_deleted' => 'Categorie is succesvol verwijderd.',
    'vehicle_category_is_used' => 'Categorie wordt gebruikt door voertuig modellen. Kan niet verwijderd worden',

    'employee_created' => 'Werknemer is succesvol aangemaakt.',
    'employee_modified' => 'Werknemer is succesvol bewerkt.',
    'employee_archived' => 'Werknemer is succesvol gearchiveerd.',

    'customer_created' => 'Klant is succesvol aangemaakt.',
    'customer_modified' => 'Klant is succesvol bewerkt.',
    'customer_archived' => 'Klant is succesvol gearchiveerd.',

    'client_created' => 'Cliënt is succesvol aangemaakt.',
    'client_modified' => 'Cliënt is succesvol bewerkt.',
    'client_archived' => 'Cliënt is succesvol gearchiveerd.',

    'user_cant_archive_himself' => "U kan niet uzelf archiveren.",

    'role_created' => 'Rol is succesvol aangemaakt.',
    'role_modified' => 'Rol is succesvol bewerkt.',
    'role_deleted' => 'Rol is succesvol verwijderd.',
    'role_deleted_denied' => 'Rol wordt gebruikt door gebruikers. Kan niet verwijderd worden.',

    'empty_ticket_list' => 'U heeft geen tickets geselecteerd.',
    'tickets_archived' => 'Ticket(s) zijn succesvol gearchiveerd.',
    'tickets_bookmarked' => 'Ticket(s) zijn succesvol gemarkeerd.',

    'ticket_created' => 'Ticket is succesvol aangemaakt, hij zal zo spoedig mogelijk verwerkt worden.',

    'support_category_created' => 'Categorie is succesvol aangemaakt.',
    'support_category_modified' => 'Categorie is succesvol bewerkt.',
    'support_category_deleted' => 'De categorie en zijn sub categorieën is succesvol verwijderd.',

    'support_sub_category_created' => 'Sub categorie is succesvol aangemaakt.',
    'support_sub_category_modified' => 'Sub categorie is succesvol bewerkt.',
    'support_sub_category_delete' => 'Sub categorie is succesvol verwijderd.',

    'support_priority_created' => 'Prioriteit is succesvol aangemaakt.',
    'support_priority_modified' => 'Prioriteit is succesvol bewerkt.',
    'support_priority_deleted' => 'Prioriteit is succesvol verwijderd.',

    'support_bookmark_created' => 'Markering is succesvol aangemaakt.',
    'support_bookmark_modified' => 'Markering is succesvol bewerkt.',
    'support_bookmark_deleted' => 'Markering is succesvol verwijderd.',
];
