<?php

return [

    'error_404_quote' => 'I like to think that even if we make some really bad choices and go down some bad paths, we\'ll eventually emerge from it.',

    'your_email' => 'Your email',
    'your_password' => 'Your password',

    'dashboard' => 'Dashboard',
    'calendar' => 'Calendar',
    'vehicle_management' => 'Vehicle man.',
    'vehicles' => 'Vehicles',
    'categories' => 'Categories',
    'garage' => 'Garage',
    'repairs' => 'Repairs',
    'apk' => 'APK',
    'materials' => 'Materials',
    'users' => 'Users',
    'roles' => 'Roles',
    'news' => 'News',
    'files' => 'Files',
    'support' => 'Support',
    'tickets' => 'Tickets',
    'activity_log' => 'Activity log',

    'overview' => 'Overview',
    'management' => 'Management',

    'employees' => 'Employees',
    'employee' => 'Employee',

    'customers' => 'Customers',
    'customer' => 'Customer',

    'clients' => 'Clients',
    'client' => 'Client',

    'notifications' => 'Notifications',
    'profile' => 'Profile',
    'logout' => 'Log out',
    'my_account' => 'My account',
    'my_profile' => 'My profile',

    'add' => 'Add',
    'create' => 'Create',
    'edit' => 'Edit',
    'update' => 'Update',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'delete' => 'Delete',
    'actions' => 'Actions',
    'search' => 'Search',
    'access' => 'Access',
    'print' => 'Print',
    'send' => 'Send',
    'reset' => 'Reset',

    'yes' => 'Yes',
    'no' => 'No',

    'next_step' => 'Next step',
    'back' => 'Back',

    'email' => 'Email',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'address' => 'Address',
    'address_information' => 'Address information',
    'city' => 'City',
    'zipcode' => 'Zipcode',
    'state' => 'State',
    'province' => 'Province',
    'country' => 'Country',
    'phone' => 'Phone no.',
    'mobile' => 'Mobile no.',
    'bsn_number' => 'BSN number',
    'birthday' => 'Birthday',
    'date_of_service' => 'Date of service',
    'date_of_subscription' => 'Date of subscription',
    'password' => 'Password',
    'old_password' => 'Old password',
    'new_password' => 'New password',
    'repeat_new_password' => 'Repeat new password',
    'repeat_password' => 'Repeat password',
    'reset_password' => 'Reset password',
    'forgot_password' => 'Forgot password?',
    'reset_password_link_send' => '',

        'title' => 'Title',
    'description' => 'Description',
    'content' => 'Content',

    'posted_by' => 'Posted by',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',

    'avatar' => 'Avatar',
    'profile_photo_text' => 'Profile photo will be changed automatically',
    'upload_new_photo' => 'Upload new photo',

    'personal_data' => 'Personal data',

    'number' => 'Number',

    'vehicle_data' => 'Vehicle data',
    'license_plate' => 'License plate',
    'purchase_date' => 'Purchase date',
    'sales_date' => 'Sales date',
    'under_repair' => 'Under repair',
    'next_apk' => 'Next APK',

    'note' => 'Note',

    'name' => 'Name',

    'driver_license' => 'Driver license',

    'brands' => 'Brands',
    'brand' => 'Brand',

    'models' => 'Models',
    'model' => 'Model',

    'types' => 'Types',
    'type' => 'Type',

    'vehicle' => 'Vehicle',
    'create_vehicle' => 'Create vehicle',
    'edit_vehicle' => 'Edit vehicle',
    'create_vehicles' => 'Create vehicle',
    'edit_vehicles' => 'Edit vehicle',
    'delete_vehicles' => 'Delete vehicles',

    'create_vehicle_brand' => 'Create vehicle brand',
    'edit_vehicle_brand' => 'Edit vehicle  brand',

    'create_vehicle_model' => 'Create vehicle model',
    'edit_vehicle_model' => 'Edit vehicle model',

    'create_vehicle_category' => 'Create vehicle category',
    'edit_vehicle_category' => 'Edit vehicle category',

    'create_material' => 'Create material',

    //USER
    'create_user' => 'Create user',
    'reset_user_password' => 'Reset Password',
    'want_password_reset' => 'Do you want to reset your password?',

    'create_employee' => 'Create employee',
    'edit_employee' => 'Edit employeee',

    'create_customer' => 'Create customer',
    'edit_customer' => 'Edit customer',

    'create_client' => 'Create client',
    'edit_client' => 'Edit client',

    'role' => 'Role',
    'create_role' => 'Create role',
    'edit_role' => 'Edit role',

    'create_news' => 'Create news item',
    'news_items' => 'News items',

    'new_category' => 'New category',
    'category' => 'Category',
    'access_categories' => 'Access categories',
    'create_categories' => 'Create categories',
    'edit_categories' => 'Edit categories',
    'delete_categories' => 'Delete categories',
    'new_sub_category' => 'New subcategory',
    'sub_categories' => 'Subcategories',
    'sub_category' => 'Subcategory',
    'create_sub_categories' => 'Create subcategories',
    'edit_sub_categories' => 'Edit subcategories',
    'delete_sub_categories' => 'Delete subcategories',

    'selected' => 'Selected',
    'sort_on' => 'Sort on',
    'select' => 'Select',
    'deselect' => 'Deselect',

    'create_support_category' => 'Create category',
    'create_support_sub_category' => 'Create subcategory',
    'create_support_priority' => 'Create priority',
    'create_support_bookmark' => 'Create bookmark',

    'edit_support_category' => 'Edit category',
    'edit_support_sub_category' => 'Edit subcategory',
    'edit_support_priority' => 'Edit priority',
    'edit_support_bookmark' => 'Edit bookmark',

    'delete_support_priority' => 'Delete priority',
    'delete_support_bookmark' => 'Delete bookmak',

    'see_results' => 'See results',

    'bookmark' => 'Mark',
    'bookmarking' => 'Bookmarking',
    'bookmarks' => 'Bookmarks',
    'mark_as' => 'Mark as',
    'marked_as' =>'Marked as',
    'to_mark' => 'Markeren',

    'active' => 'Active',
    'archive' => 'Archive',
    'archiving' => 'Archiving',

    'info' => 'Information',

    'bulk_actions' => 'Bulk actions',
    'export' => 'Export',

    'show_only' => 'Show only',
    'all' => 'All',
    'solved' => 'Solved',
    'unsolved' => 'Unsolved',

    'filter_segments' => 'Filter segments',
    'global' => 'Global',
  	'related' => 'Related',
    'related_to' => 'Related to',

    'priority' => 'Priority',
    'priorities' => 'Priorities',

    'urgent' => 'Urgent',
    'high' => 'High',
    'medium' => 'Medium',
    'low' => 'Low',

    'status' => 'Status',

    'ticket' => 'Ticket',
    'new_ticket' => 'New ticket',
    'my_tickets' => 'My tickets',
    'create_ticket' => 'Create ticket',

    'expiry_date' => 'Due in',

    'stock' => 'Stock',

    //LOG
    'executor' => 'Executor',
    'activity' => 'Activity',
    'related_user_id' => 'related user id',
    'section_model' => 'Section model',
    'section_target' => 'Section target',
];
