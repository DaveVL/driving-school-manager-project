<?php
return [
    'delete_vehicle_part_one_warning' => 'Are you sure you want to delete the vehicle with license plate: ',
    'delete_vehicle_part_two_warning' => '?',

    'delete_vehicle_brand_part_one_warning' => 'Are you sure you want to delete the brand ',
    'delete_vehicle_brand_part_two_warning' => '?',

    'delete_vehicle_model_part_one_warning' => 'Are you sure you want to delete the model ',
    'delete_vehicle_model_part_two_warning' => '?',

    'delete_vehicle_category_part_one_warning' => 'Are you sure you want to delete the category ',
    'delete_vehicle_category_part_two_warning' => '?',

    'archive_user_part_one_warning' => 'Are you sure you want to archive ',
    'archive_user_part_two_warning' => '? Click yes to proceed.',

    'archive_ticket_part_one' => 'Are you sure you want to archive the underlying tickets? ',
    'archive_ticket_part_two' => 'Click yes to archive the tickets',

    'delete_role_part_one_warning' => 'Are you sure you want to delete the role ',
    'delete_role_part_two_warning' => '. When deleted, it is not possible to retrieve the role.',

    'delete_support_category_part_one_warning' => 'Are you sure you want to delete the category ',
    'delete_support_category_part_two_warning' => '? The related sub categories will also be deleted.',

    'delete_support_sub_category_part_one_warning' => 'Are you sure you want to delete the sub category ',
    'delete_support_sub_category_part_two_warning' => '?',

    'delete_support_priority_part_one_warning' => 'Are you sure you want to delete the priority ',
    'delete_support_priority_part_two_warning' => '?',

    'delete_support_bookmark_part_one_warning' => 'Are you sure you want to delete the bookmark ',
    'delete_support_bookmark_part_two_warning' => '?',
];
