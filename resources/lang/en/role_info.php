<?php

return [
  'vehicles_access_info' => 'Role can access the vehicles page and view vehicles.',
  'vehicles_create_info' => 'Role can create vehicles.',
  'vehicles_edit_info' => 'Role can edit vehicles.',
  'vehicles_delete_info' => 'Role can delete vehicles.',

  'vehicle_categories_access_info' => 'Role can access the vehicle categories page and view categories.',
  'vehicle_categories_create_info' => 'Role can create vehicle categories.',
  'vehicle_categories_edit_info' => 'Role can edit vehicle categories.',
  'vehicle_categories_delete_info' => 'Role can archive vehicle categories.',

  'materials_access_info' => 'Role can access the materials page and view materials.',
  'materials_create_info' => 'Role can create materials.',
  'materials_edit_info' => 'Role can edit materials.',
  'materials_delete_info' => 'Role can delete materials.',

  'users_access_info' => 'Role can access the users page and view users.',
  'users_create_info' => 'Role can create users.',
  'users_edit_info' => 'Role can edit users.',
  'users_archive_info' => 'Role can archive users.',

  'news_access_info' => 'Role can access the news page and view news content.',
  'news_create_info' => 'Role can create news.',
  'news_edit_info' => 'Role can edit news.',
  'news_delete_info' => 'Role can delete news.',

  'files_access_info' => 'Role can access the files page and view files.',
  'files_create_info' => 'Role can create files',
  'files_edit_info' => 'Role can edit files',
  'files_delete_info' => 'Role can delete files.',

  'support_overview_access_info' => 'Role can access the overview page.',
  'support_management_access_info' => 'Role can access the management page.',
  'support_edit_info' => 'Role can edit tickets.',
  'support_archive_info' => 'Role can archive tickets.',

  'support_create_category_info' => 'Role can create support categories.',
  'support_edit_category_info' => 'Role can edit support categories',
  'support_delete_category_info' => 'Role can delete support categories.',

  'support_create_sub_category_info' => 'Role can create support sub categories.',
  'support_edit_sub_category_info' => 'Role can edit support sub categories',
  'support_delete_sub_category_info' => 'Role can delete support sub categories.',

  'support_create_priority_info' => 'Role can create support priorities.',
  'support_edit_priority_info' => 'Role can edit support priorities',
  'support_delete_priority_info' => 'Role can delete support priorities.',

  'support_create_bookmark_info' => 'Role can create support bookmarks.',
  'support_edit_bookmark_info' => 'Role can edit support bookmarks',
  'support_delete_bookmark_info' => 'Role can delete support bookmarks.',

  'tickets_access_info' => 'Role can access the ticket page and view his own tickets.',
  'tickets_create_info' => 'Role can create tickets',

  'activity_log_access_info' => 'Role can access the actiity log page and view activity logs.',
  'activity_log_print_info' => 'Role can print activity logs',
];
