<?php

return [
    'illegal_conventions' => 'Illegal conventions used.',
    'unexpected_error' => 'Process failed. Unexpected error.',

    'my_profile_updated' => 'Your profile is succesfully updated.',

    'vehicle_created' => 'Vehicle is successfully created.',
    'vehicle_modified' => 'Vehicle is successfully modified.',
    'vehicle_deleted' => 'Vehicle is successfully deleted.',

    'vehicle_brand_created' => 'Brand is successfully created.',
    'vehicle_brand_modified' => 'Brand is successfully modified.',
    'vehicle_brand_deleted' => 'Brand is successfully deleted.',
    'vehicle_brand_is_used' => 'Brand is used by vehicle models. It cannot be deleted.',

    'vehicle_model_created' => 'Model is successfully created.',
    'vehicle_model_modified' => 'Model is successfully modified.',
    'vehicle_model_deleted' => 'Model is successfully deleted.',
    'vehicle_model_is_used' => 'Vehicle model is used by vehicles. It cannot be deleted.',

    'vehicle_category_created' => 'Vehicle category is successfully created.',
    'vehicle_category_modified' => 'Vehicle category is successfully modified.',
    'vehicle_category_deleted' => 'Vehicle category is successfully deleted.',
    'vehicle_category_is_used' => 'Vehicle category is used by vehicle model(s). It cannot be deleted.',

    'employee_created' => 'Employee is successfully created.',
    'employee_modified' => 'Employee is successfully modified.',
    'employee_archived' => 'Employee is successfully archived.',

    'customer_created' => 'Customer is successfully created.',
    'customer_modified' => 'Customer is successfully modified.',
    'customer_archived' => 'Customer is successfully archived.',

    'client_created' => 'Client is successfully created.',
    'client_modified' => 'Client is successfully modified.',
    'client_archived' => 'Client is successfully archived.',

    'user_cant_archive_himself' => "You can't archive yourself",

    'role_created' => 'Role is successfully created.',
    'role_modified' => 'Role is successfully modified.',
    'role_deleted' => 'Role is successfully deleted.',
    'role_deleted_denied' => 'It is not possible to delete the role. Users are using it.',

    'empty_ticket_list' => 'There are no tickets selected.',
    'tickets_archived' => 'Ticket(s) are successfully archived.',

    'ticket_created' => 'Ticket is successfully created, it will be processed as soon as possible.',

    'support_category_created' => 'Category is successfully created.',
    'support_category_modified' => 'Category is successfully modified.',
    'support_category_deleted' => 'The Category and his sub categories is successfully deleted.',

    'support_sub_category_created' => 'Sub category is succesfully created.',
    'support_sub_category_modified' => 'Sub category is sucessfully modified.',
    'support_sub_category_delete' => 'Sub category is succesfully deleted.',

    'support_priority_created' => 'Priority is successfully created.',
    'support_priority_modified' => 'Priority is successfully modified.',
    'support_priority_deleted' => 'Priority is successfully deleted.',

    'support_bookmark_created' => 'Bookmark is successfully created.',
    'support_bookmark_modified' => 'Bookmark is successfully modified.',
    'support_bookmark_deleted' => 'Bookmark is successfully deleted.',
];
