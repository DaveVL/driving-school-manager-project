<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | {{  Route::currentRouteName() }}</title>

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

</head>

<body class="fixed-sn white-skin">

<!--Main Navigation-->
<header>
    <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed sn-bg-1 custom-scrollbar">
        <!-- Logo -->
        <li class="logo-sn waves-effect">
            <div class="text-center avatar ">
                <img src="/img/uploads/avatars/{{ $profile->avatar }}" class="rounded-circle" height="92" width="92">
                <p class="user text-center black-text">{{ $profile->firstname  }} {{ $profile->lastname }}</p>
            </div>
        </li>
        <!--/. Logo -->

        <!--Search Form-->
        <li>
            <form class="search-form" role="search">
                <div class="form-group waves-effect">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
        </li>
        <!--/.Search Form-->
        <!-- Side navigation links -->
        <li>
            <ul class="collapsible collapsible-accordion">
                <li><a href="/dashboard" class="collapsible-header waves-effect arrow-r"><i
                                class="fa fa-dashboard"></i> {{ trans('messages.dashboard') }}</a>
                @if($rolePermissions->calendar_access == true)
                    <li><a href="/calendar" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-calendar"></i> {{ trans('messages.calendar') }}</a>
                @endif
                @if($rolePermissions->vehicles_access == true)
                    <li><a href="/vehicles" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-car"></i> {{ trans('messages.vehicle_management') }}</a>
                @endif
                @if($rolePermissions->materials_access == true)
                    <li><a class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-wrench"></i> {{ trans('messages.garage') }}<i
                                    class="fa fa-angle-down rotate-icon"></i></a>
                        <div class="collapsible-body">
                            <ul>
                                @if($rolePermissions->materials_access == true)
                                    <li><a href="/materials" class="waves-effect"> {{ trans('messages.repairs') }}</a>
                                    </li>
                                @endif
                                @if($rolePermissions->materials_access == true)
                                    <li><a href="/materials" class="waves-effect"> {{ trans('messages.apk') }}</a>
                                    </li>
                                @endif
                                @if($rolePermissions->materials_access == true)
                                    <li><a href="/materials" class="waves-effect"> {{ trans('messages.materials') }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if($rolePermissions->employees_access == true || $rolePermissions->customers_access == true ||
                    $rolePermissions->clients_access == true )
                    <li><a href="/users" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-users"></i> {{ trans('messages.users') }}</a>
                @endif
                @if($rolePermissions->roles_access == true)
                    <li><a href="/roles" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-star"></i> {{ trans('messages.roles') }}</a>
                @endif
                @if($rolePermissions->news_access == true)
                    <li><a href="/news" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-newspaper-o"></i> {{ trans('messages.news') }}</a>
                @endif
                @if($rolePermissions->support_overview_access == true || $rolePermissions->support_management_access == true)
                    <li><a class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-support"></i> {{ trans('messages.support') }}<i
                                    class="fa fa-angle-down rotate-icon"></i></a>
                        <div class="collapsible-body">
                            <ul>
                                @if($rolePermissions->support_overview_access == true)
                                    <li><a href="/support/overview"
                                           class="waves-effect"> {{ trans('messages.overview') }}</a>
                                    </li>
                                @endif
                                @if($rolePermissions->support_management_access == true)
                                    <li><a href="/support/management"
                                           class="waves-effect"> {{ trans('messages.management') }}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </li>
                @endif
                @if($rolePermissions->tickets_access == true)
                    <li><a href="/tickets" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-ticket"></i> {{ trans('messages.tickets') }}</a>
                @endif
                @if($rolePermissions->files_access == true)
                    <li><a href="/files" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-file"></i> {{ trans('messages.files') }}</a>
                @endif
                @if($rolePermissions->activity_log_access == true)
                    <li><a href="/activity-log" class="collapsible-header waves-effect arrow-r"><i
                                    class="fa fa-user-secret"></i> {{ trans('messages.activity_log') }}</a>
                @endif
            </ul>
        </li>
        <!--/. Side navigation links -->
        <div class="sidenav-bg mask-strong"></div>
    </ul>
    <!--/. Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav"
         style="height: 55px">
        <!-- SideNav slide-out button -->
        <div class="float-left">
            <a href="#" data-activates="slide-out" class="button-collapse black-text"><i class="fa fa-bars"></i></a>
        </div>

        <!-- Breadcrumb-->
    @if (Breadcrumbs::exists())
        {{  Breadcrumbs::render() }}
    @endif

    <!--Navbar links-->
        <ul class="nav navbar-nav nav-flex-icons ml-auto">

            <!-- Dropdown -->
            <li class="nav-item dropdown notifications-nav">
                <a class="nav-link dropdown-toggle waves-effect" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true"
                   aria-expanded="false">
                    <span class="badge green white-text">0</span> <i class="fa fa-bell"></i>
                    <span class="d-none d-md-inline-block">{{ trans('messages.notifications') }}</span>
                </a>
                <div class="dropdown-menu dropdown-info" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">
                        <i class="fa fa-money mr-2" aria-hidden="true"></i>
                        <span>New order received</span>
                        <span class="float-right"><i class="fa fa-clock-o" aria-hidden="true"></i> 13 min</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link waves-effect"><i class="fa fa-comments-o"></i> <span
                            class="clearfix d-none d-sm-inline-block">{{ trans('messages.support') }}</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle waves-effect" href="#" id="userDropdown" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user"></i> <span
                            class="clearfix d-none d-sm-inline-block">{{ __('messages.profile') }}</span></a>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="/account/logout">{{ trans('messages.logout') }}</a>
                    <a class="dropdown-item" href="/account/profile">{{ trans('messages.my_account') }}</a>
                </div>
            </li>

        </ul>
        <!--/Navbar links-->
    </nav>
    <!-- /.Navbar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main>
    <div class="container-fluid">
        @yield('content')
    </div>
</main>
<!--Main layout-->

<!-- Scripts -->
<script src="{{ asset('./js/all.js') }}"></script>
<script src="{{ asset('./js/main.js') }}"></script>

<script type="text/javascript">
    @if(Session::has('info'))
$(document).ready(function () {
        toastr.info('{{ Session::get('info') }}');
    });
    @endif

    @if(Session::has('success'))
$(document).ready(function () {
        toastr.success('{{ Session::get('success') }}');
    });
    @endif

    @if(Session::has('status'))
$(document).ready(function () {
        toastr.success('{{ Session::get('status') }}');
    });
    @endif

    @if(Session::has('warning'))
$(document).ready(function () {
        toastr.warning('{{ Session::get('warning') }}');
    });
    @endif

    @if(Session::has('error'))
$(document).ready(function () {
        toastr.error('{{ Session::get('error') }}');
    });
    @endif
</script>

</body>

</html>
