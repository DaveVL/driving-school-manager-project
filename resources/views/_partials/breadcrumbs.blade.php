@if (count($breadcrumbs))

    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb-item breadcrumb-dn mr-auto"><a href="{{ $breadcrumb->url }}" class="grey-text">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="breadcrumb-item active breadcrumb-dn mr-auto">{{ $breadcrumb->title }}</li>
            @endif

        @endforeach
    </ol>

@endif
