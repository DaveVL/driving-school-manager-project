@extends('layouts.app')

@section('content')
<!--Section: Team v.1-->
  <section class="section dashboard-section">
    <div class="row">
      <div class="col-md-3 mb-r">
        <div class="card card-cascade narrower">
          <!--Card image-->
          <div class="view gradient-card-header peach-gradient">
              <h4 class="mb-0"> Quick taps </h4>
          </div>
          <!--/Card image-->

          <!--Card content-->
          <div class="card-body text-center">
            <a class="btn-floating btn-secondary waves-effect waves-light" href="/tickets/create"><i class="fa fa-ticket"></i></a>
            <a class="btn-floating btn-secondary waves-effect waves-light" href="/tickets/create"><i class="fa fa-car"></i></a>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 mb-r">
        <div class="card card-cascade narrower">

              <!--Card image-->
              <div class="view gradient-card-header peach-gradient">
                  <h4 class="mb-0"> Recente activiteiten </h4>
              </div>
              <!--/Card image-->

              <!--Card content-->
              <div class="card-body text-center">

                <table class="table no-header">
                    <tbody>
                        @if(isset($logs))
                            @foreach($logs as $log)
                            <tr>
                                <td>{{ $log->user->firstname . ' ' . $log->user->lastname }}</td>
                                <td>{{ $log->activity }}</td>
                                <td class="hour"><small><span class="grey-text float-right"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $log->created_at }}</span></small></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>

              </div>
              <!--/.Card content-->

          </div>
      </div>
    </div>
  </section>
  <section class="section extra-margins text-center pb-3">

      <!--Grid row-->
      <div class="row">
          <div class="col-lg-6 col-md-6 mb-r">

          </div>
          <!--Grid column-->
          <div class="col-lg-6 col-md-6 mb-r">
                        <!--Carousel Wrapper-->
            <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
              <!--Indicators-->
              <ol class="carousel-indicators">
                  <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-2" data-slide-to="1"></li>
                  <li data-target="#carousel-example-2" data-slide-to="2"></li>
              </ol>
              <!--/.Indicators-->
              <!--Slides-->
              <div class="carousel-inner" role="listbox">
                <a href="lol" class="white-text">
                  <div class="carousel-item active">
                      <div class="view hm-black-light">
                          <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg" alt="First slide">
                          <div class="mask"></div>
                      </div>
                      <div class="carousel-caption">
                          <h3 class="h3-responsive">Update 0.1.1</h3>
                          <p> door Dave van luling </p>
                      </div>
                    </a>
                  </div>
                  <div class="carousel-item">
                      <!--Mask color-->
                      <div class="view hm-black-strong">
                          <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(6).jpg" alt="Second slide">
                          <div class="mask"></div>
                      </div>
                      <div class="carousel-caption">
                          <h3 class="h3-responsive">Strong mask</h3>
                          <p>Secondary text</p>
                      </div>
                  </div>
                  <div class="carousel-item">
                      <!--Mask color-->
                      <div class="view hm-black-slight">
                          <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(9).jpg" alt="Third slide">
                          <div class="mask"></div>
                      </div>
                      <div class="carousel-caption">
                          <h3 class="h3-responsive">Slight mask</h3>
                          <p>Third text</p>
                      </div>
                  </div>
              </div>
              <!--/.Slides-->
              <!--Controls-->
              <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>
              <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->
          </div>
          <!--Grid column-->

      </div>
      <!--Grid row-->

  </section>
@endsection
