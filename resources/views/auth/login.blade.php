<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="full-height">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name') }} | Login</title>

    <!-- SASS !-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Your custom styles (optional) -->
    <style>
        .intro-2 {
            background: url("/img/backgrounds/alaska_background.jpg") no-repeat center center;
            background-size: cover;
        }

        @media (max-width: 768px) {
            .navbar:not(.top-nav-collapse) {
                background: #3f51b5 !important;
            }
        }

        .card {
            background-color: rgba(229, 228, 255, 0.2);
        }

        .md-form .prefix {
            font-size: 1.5rem;
            margin-top: 1rem;
        }

        .md-form label {
            color: #ffffff;
        }

        h6 {
            line-height: 1.7;
        }

        @media (max-width: 740px) {
            .full-height,
            .full-height body,
            .full-height header,
            .full-height header .view {
                height: 750px;
            }
        }

        @media (min-width: 741px) and (max-height: 638px) {
            .full-height,
            .full-height body,
            .full-height header,
            .full-height header .view {
                height: 750px;
            }
        }

        .card {
            margin-top: 30px;
            /*margin-bottom: -45px;*/

        }

        .md-form input[type=text]:focus:not([readonly]),
        .md-form input[type=password]:focus:not([readonly]) {
            border-bottom: 1px solid #8EDEF8;
            box-shadow: 0 1px 0 0 #8EDEF8;
        }

        .md-form input[type=text]:focus:not([readonly]) + label,
        .md-form input[type=password]:focus:not([readonly]) + label {
            color: #8EDEF8;
        }

        .md-form .form-control {
            color: #fff;
        }

    </style>
</head>


<body class="fixed-sn white-skin">

<!--Main Navigation-->
<header>

    <!--Intro Section-->
    <section class="view intro-2 hm-stylish-strong">
        <div class="full-bg-img flex-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-lg-5">

                        <!--Form with header-->
                        <div class="card wow fadeIn" data-wow-delay="0.3s">
                            <div class="card-body">

                            {!! Form::open(['route' =>  'login', 'method' => 'post', 'class' => 'form-horizontal', 'name' => 'login-form']) !!}
                            <!--Header-->
                                <div class="form-header peach-gradient">
                                    <h3><i class="fa fa-user mt-2 mb-2"></i> Log in:</h3>
                                </div>

                                <!--Body-->
                                <div class="md-form">
                                    <i class="fa fa-envelope prefix white-text"></i>
                                    {{ Form::email('email', old('email'), ['class' => 'form-control validate' . ($errors->has('email') ? ' invalid mb-3' : ''), 'id' => 'email', 'maxlength' => '254', 'required']) }}
                                    {{ Form::label('email', trans('messages.your_email'), ['data-error' => $errors->first('email')]) }}
                                </div>

                                <div class="md-form">
                                    <i class="fa fa-lock prefix white-text"></i>
                                    {{ Form::password('password', ['class' => 'form-control validate' . ($errors->has('password') ? ' invalid mb-3' : ''), 'id' => 'password', 'required']) }}
                                    {{ Form::label('password', trans('messages.your_password'), ['data-error' => $errors->first('password')]) }}
                                </div>

                                <div class="text-center">
                                    {{--<button class="btn btn-secondary btn-lg"></button>--}}
                                    {{ Form::submit('Log in', ['class' => 'btn btn-secondary btn-lg', 'id' => 'login-form'] ) }}
                                </div>
                                <hr>
                                <div class="inline-ul text-center d-flex justify-content-left">
                                    <a style="color: white; float: left" href="{{ route('password.request') }}">{{ trans('messages.forgot_password') }}</a>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!--/Form with header-->
                    </div>
                </div>
            </div>
        </div>
    </section>

</header>
<!--Main Navigation-->

<!-- SCRIPTS -->
<!-- MDB core JavaScript -->
<script src="js/all.js"></script>
<script>
    new WOW().init();
</script>
</body>
</html>
