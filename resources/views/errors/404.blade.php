<html>
<head>
    <link rel="stylesheet" type="text/css"  href="/css/app.css"/>
    <link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            background-color: black;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }

        .text {
            font-size: 46px;
            margin-bottom: 40px;
            color: white;
        }
        .tim:hover {
          color: green;
        }
    </style>
</head>
<body onload="timer()">
<div class="container">
    <div class="content">
        <div class="title">
            {{ __('messages.error_404_quote') }}
        </div>
        <a href="https://nl.wikipedia.org/wiki/Tim_O%27Reilly" target="_blank" class="tim"><div class="text tim"> Tim O'Reilly</div></a>

        <div><img src="/img/tim-oreilly.jpg"/></div>
        <div class="text" id="timer"></div>
    </div>
</div>

  <script src="{{ asset('./js/all.js') }}"></script>
  <script>
    var count=10;

    var counter=setInterval(timer, 1000);

    function timer()
    {
    count=count-1;

      if (count <= 0)
      {

         clearInterval(counter);

        if (count == 0) {
            document.getElementById('timer').innerHTML = "Goodbye!";
          window.open("/login", "_self");
        }

         return;
      }

      if (count % 2 == 0) {
        document.getElementById('timer').style.color = 'white';
      }
      else {
        document.getElementById('timer').style.color = 'orange';
      }

      document.getElementById('timer').innerHTML = count;
    }
  </script>
</body>
</html>
