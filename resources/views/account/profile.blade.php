@extends('layouts.app')

@section('content')
    <!-- Section: Edit Account -->
    <section class="section">
        <!-- First row -->
        <div class="row">
            <!-- First column -->
            <div class="col-lg-4 mb-r">

                <!--Card-->
                <div class="card card-cascade narrower">

                    <!--Card image-->
                    <div class="view gradient-card-header peach-gradient">
                        <h4 class="mb-0">{{ trans('messages.avatar') }}</h4>
                    </div>
                    <!--/Card image-->

                    <!-- Card content -->
                    <div class="card-body text-center">
                        <img src="/img/uploads/avatars/{{ $user->avatar }}" alt="User Photo" height="150" width="150"
                             class="z-depth-1 mb-3 mx-auto"/>

                        <p class="text-muted">
                            <small>{{ trans('messages.profile_photo_text') }}</small>
                        </p>

                        {!! Form::open(['id' => 'avatar-form', 'files' => true]) !!}
                        {{ Form::file('file', ['class' => 'avatar-img-btn', 'id' => 'avatar-image', 'accept' => 'image/jpeg'])}}
                        {{ Form::label('avatar-image', trans('messages.upload_new_photo'), ['class' => 'btn btn-secondary avatar-image-label']) }}
                        {!! Form::close() !!}

                        {!! Form::open(['action' => 'AccountController@deleteAvatar', 'method' => 'post']) !!}
                        {{ Form::button(trans('messages.delete'), ['class' => 'btn btn-danger', 'id' => 'delete-avatar-btn'] ) }}
                        {!! Form::close() !!}
                    </div>
                    <!-- /.Card content -->

                </div>
                <!--/.Card-->
                <div class="card card-cascade narrower">

                    <!-- Card content -->
                    <div class="card-body text-center">

                        <p class="text-muted">
                            <small>{{ trans('messages.want_password_reset') }}</small>
                        </p>

                        {!! Form::open(['id' => 'reset', 'url' => 'account/password/reset', 'method' => 'POST']) !!}

                        <div class="md-form">
                            {{ Form::password('old_password', ['class' => 'form-control' . ($errors->has('old_password') ? ' validate invalid mb-3' : ''), 'id' => 'old_password', 'required']) }}
                            {{ Form::label('old_password', trans('messages.old_password'), ['data-error' => $errors->first('old_password')]) }}
                        </div>

                        <div class="md-form">
                            {{ Form::password('password', ['class' => 'form-control' . ($errors->has('password') ? ' invalid mb-3' : ''), 'id' => 'password', 'required']) }}
                            {{ Form::label('password', trans('messages.new_password'), ['data-error' => $errors->first('password')]) }}
                        </div>

                        <div class="md-form">
                            {{ Form::password('password_confirmation', ['class' => 'form-control' . ($errors->has('password_confirmation') ? ' invalid mb-3' : ''), 'id' => 'password-confirmation', 'required']) }}
                            {{ Form::label('password-confirmation', trans('messages.repeat_new_password'), ['data-error' => $errors->first('password_confirmation')]) }}
                        </div>

                        {{ Form::submit(trans('messages.reset_user_password'), ['class' => 'btn btn-secondary', 'id' => 'reset_user_password'] ) }}
                        {!! Form::close() !!}
                    </div>
                    <!-- /.Card content -->

                </div>
                <!--/.Card-->
            </div>
            <!-- /.First column -->
            <!-- Second column -->
            <div class="col-lg-8 mb-r">

                <!--Card-->
                <div class="card card-cascade narrower">

                    <!--Card image-->
                    <div class="view gradient-card-header peach-gradient">
                        <h4 class="mb-0">{{ __('messages.personal_data') }}</h4>
                    </div>
                    <!--/Card image-->

                    <!-- Card content -->
                    <div class="card-body text-center">
                        <!-- Edit Form -->
                    {!! Form::open(['action' => 'AccountController@updateProfile', 'method' => 'post']) !!}
                    <!--First row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-12">
                                <div class="md-form">
                                    {{ Form::email('email', $user->email, ['class' => 'form-control validate' . ($errors->has('email') ? ' invalid mb-3' : ''), 'maxlength' => '254', 'required']) }}
                                    {{ Form::label('email', trans('messages.email'), ['data-error' => $errors->first('email')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.First row-->
                        <!--Second row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('firstname', $user->firstname, ['class' => 'form-control validate' . ($errors->has('firstname') ? ' invalid mb-3' : ''), 'maxlength' => '35', 'required']) }}
                                    {{ Form::label('firstname', trans('messages.first_name'), ['data-error' => $errors->first('firstname')]) }}
                                </div>
                            </div>
                            <!--Second column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('lastname', $user->lastname, ['class' => 'form-control validate' . ($errors->has('lastname') ? ' invalid mb-3' : ''), 'maxlength' => '35', 'required']) }}
                                    {{ Form::label('lastname', trans('messages.last_name'), ['data-error' => $errors->first('lastname')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.Second row-->
                        <!--Third row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-12">
                                <div class="md-form">
                                    {{ Form::text('address', $user->address, ['class' => 'form-control validate' . ($errors->has('address') ? ' invalid mb-3' : ''), 'maxlength' => '100', 'required']) }}
                                    {{ Form::label('address', trans('messages.address'), ['data-error' => $errors->first('address')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.Third row-->
                        <!--Fourth row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('city', $user->city, ['class' => 'form-control validate' . ($errors->has('city') ? ' invalid mb-3' : ''), 'maxlength' => '60', 'required']) }}
                                    {{ Form::label('city', trans('messages.city'), ['data-error' => $errors->first('city')]) }}
                                </div>
                            </div>
                            <!--Second column-->
                            <div class="col-md-3">
                                <div class="md-form">
                                    {{ Form::text('state', $user->state, ['class' => 'form-control validate' . ($errors->has('state') ? ' invalid mb-3' : ''), 'maxlength' => '50', 'required']) }}
                                    {{ Form::label('state', trans('messages.state') . '/' . __('messages.province'), ['data-error' => $errors->first('state')]) }}
                                </div>
                            </div>
                            <!--Third column-->
                            <div class="col-md-3">
                                <div class="md-form">
                                    {{ Form::text('zipcode', $user->zipcode, ['class' => 'form-control validate' . ($errors->has('zipcode') ? ' invalid mb-3' : ''), 'maxlength' => '9', 'required']) }}
                                    {{ Form::label('zipcode', trans('messages.zipcode'), ['data-error' => $errors->first('zipcode')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.Fifth row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-12">
                                <div class="md-form">
                                    {{ Form::text('country', $user->country, ['class' => 'form-control validate' . ($errors->has('country') ? ' invalid mb-3' : ''), 'maxlength' => '55', 'required']) }}
                                    {{ Form::label('country', trans('messages.country'), ['data-error' => $errors->first('country')]) }}
                                </div>
                            </div>
                        </div>
                        <!--Sixth row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('phone', $user->phone, ['class' => 'form-control validate' . ($errors->has('phone') ? ' invalid mb-3' : ''), 'maxlength' => '15', 'required']) }}
                                    {{ Form::label('phone', trans('messages.phone'), ['data-error' => $errors->first('phone')]) }}
                                </div>
                            </div>
                            <!--Second column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('mobile', $user->mobile, ['class' => 'form-control validate' . ($errors->has('mobile') ? ' invalid mb-3' : ''), 'maxlength' => '15', 'required']) }}
                                    {{ Form::label('mobile', trans('messages.mobile'), ['data-error' => $errors->first('mobile')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.Sixth row-->
                        <!--Seventh row-->
                        <div class="row">
                            <!--First column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    {{ Form::text('birthday', $user->birthday, ['class' => 'form-control datepicker' . ($errors->has('birthday') ? ' invalid mb-3' : ''), 'id' => 'birthday', 'maxlength' => '10', 'required']) }}
                                    {{ Form::label('birthday', trans('messages.birthday'), ['data-error' => $errors->first('birthday')]) }}
                                </div>
                            </div>
                        </div>
                        <!--/.Seventh row-->
                        <!-- Eight row -->
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {{ Form::submit( trans('messages.update'), ['class' => 'btn btn-success']) }}
                            </div>
                        </div>
                        <!-- /.Seventh row -->
                    {!! Form::close() !!}
                    <!-- Edit Form -->
                    </div>
                    <!-- /.Card content -->

                </div>
                <!--/.Card-->
            </div>
            <!-- /.Second column -->
        </div>
        <!-- /.First row -->
    </section>
    <!-- /.Section: Edit Account -->
@endsection
