var mdbInit = require('./local/mdb-initializiation');
var init = require('./local/initializiations');
var baseEvents = require('./local/events');
var localDashboard = require('./local/dashboard/main');
var localCalendar = require('./local/calendar/main');
var localAccount = require('./local/account/main');
var localVehicles = require('./local/vehicles/main');
var localMaterials = require('./local/materials/main');
var localUsers = require('./local/users/main');
var localRoles = require('./local/roles/main');
var localNews = require('./local/news/main');
var localSupport = require('./local/support/main');
var localTickets = require('./local/tickets/main');
var localActivityLogs = require('./local/activity-logs/main');

// Import TinyMCE
var tinymce = require('./vendor/tinymce/tinymce');

// A theme is also required
require('./vendor/tinymce/themes/modern/theme');

// Any plugins you want to use has to be imported
require('./vendor/tinymce/plugins/paste');
require('./vendor/tinymce/plugins/link');
require('./vendor/tinymce/plugins/autoresize');

$(document).ready(function() {

    mdbInit.init();
    init.init();

    baseEvents.init();
    localDashboard.init();
    localAccount.init();
    localVehicles.init();
    localMaterials.init();
    localUsers.init();
    localRoles.init();
    localNews.init();
    localSupport.init();
    localTickets.init();
    localActivityLogs.init();
    localCalendar.init();

    tinymce.init({
       selector: '.tinymce',
       theme: 'modern',
       skin_url:  '/css/skins/lightgray',
       branding: false,
       plugins: ['paste', 'link', 'autoresize']
    });
});
